import { texts } from '../data.js';

export const getRandomText = () => {
  const index = Math.floor(Math.random()*texts.length+1);
  return texts[index];
}

export const getRandomTextId = () => {
  const index = Math.floor(Math.random()*texts.length+1);
  return index;
}

export const getText = (id) => {
  return texts[id-1];
}