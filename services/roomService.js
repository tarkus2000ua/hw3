import { rooms } from '../data.js';
import { MAXIMUM_USERS_FOR_ONE_ROOM } from '../socket/config.js';

export const isUserExisted = (username) => {
  return rooms.filter(room => room.users.findIndex(user => user.name === username) !== -1).length > 0 ? true : false;
}

export const isRoomExisted = (room) => {
  return rooms.filter(r => r.name === room).length > 0 ? true : false;
}

const isRoomEmpty = (room) => {
  const roomToCheck = rooms.find(r => r.name === room);
  return (roomToCheck.users.length === 0)
}

export const removeEmptyRoom = (room) => {
  if (isRoomEmpty(room)){
    const index = rooms.findIndex(r => r.name === room);
    rooms.splice(index, 1);
    return true;
  }
  return false;
}

export const roomsInfo = () => {
  const info = rooms
    .filter(room => room.name !== 'WAITING_ROOM')
    .filter(room => room.status !== 'busy')
    .filter(room => room.users.length < MAXIMUM_USERS_FOR_ONE_ROOM)
    .map(room => {
      return {
        name: room.name,
        connected: room.users.length
      }
    });
  return info;
}

export const lockRoom = (room) => {
  const roomToLock = rooms.find(r => r.name === room);
  roomToLock.status = 'busy';
}

export const unlockRoom = (room) => {
  const roomToUnLock = rooms.find(r => r.name === room);
  roomToUnLock.status = 'free';
}

export const resetUsers = (room) => {
  const roomToReset = rooms.find(r => r.name === room);
  roomToReset.users.forEach(user => user.isready = false) ;
}

export const createRoom = (room) => {
    rooms.push({
      name:room,
      status:'free',
      users:[]
    });
} 

export const joinRoom = (user, room) => {
  // Remove user from waiting room
  const waitingRoom = rooms.find(r => r.name === 'WAITING_ROOM');
  const index = waitingRoom.users.findIndex(u => u.name === user);
    if ( index !== -1) {
      waitingRoom.users.splice(index,1);
    }

  const roomToJoin = rooms.find(r => r.name === room);
  roomToJoin.users.push({
    name: user,
    "isready": false,
    "progress": 0,
    "time":100
  });
  return roomToJoin;
}

export const exitRoom = (user, room) => {
  const roomToExit = rooms.find(r => r.name === room);
  roomToExit.users = roomToExit.users.filter(u => u.name !== user);
  return roomToExit;
}

export const updateUserInfo = (user, room, param) => {
  const roomToUpdate = rooms.find(r => r.name === room);
  const userToUpdate = roomToUpdate.users.find(u => u.name === user);
  userToUpdate[param.name] = param.value;
  return roomToUpdate;
}

export const userDisconnected = (name) => {
  const roomToExit = rooms.find(r => r.users.find(user => user.name ===  name));
  const index = roomToExit.users.findIndex(user => user.name === name);
  roomToExit.users.splice(index, 1);
  return roomToExit;
  }

export const isReadyToStart = (room) => {
  return room.users.every(user => user.isready==true);
}

export const getGameResults = (room) => {
  const roomToShowResults = rooms.find(r => r.name === room);
  const results = [...roomToShowResults.users];
  results.sort((a,b) => (b.progress - a.progress || a.time - b.time))
  return results;
}

export const checkIsEverybodyCompleted = (room) => {
  const roomToCheck = rooms.find(r => r.name === room);
  return roomToCheck.users.every(user => user.progress===100);
}