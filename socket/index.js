import {
  roomsInfo,
  joinRoom,
  exitRoom,
  isUserExisted,
  isRoomExisted,
  userDisconnected,
  isReadyToStart,
  lockRoom,
  unlockRoom,
  resetUsers,
  getGameResults,
  updateUserInfo,
  createRoom,
  removeEmptyRoom,
  checkIsEverybodyCompleted
} from '../services/roomService.js';
import {
  SECONDS_TIMER_BEFORE_START_GAME,
  SECONDS_FOR_GAME
} from '../socket/config.js';
import {
  getRandomTextId
} from '../services/textService.js';

let roomInfo;

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
    let timer;

    if (isUserExisted(username)) {
      socket.emit('userExisted', `Username ${username} already existed on server. Please choose another name.`);
    } else {
      socket.join('WAITING_ROOM');
      joinRoom(username, 'WAITING_ROOM');
      socket.emit('roomsInfo', roomsInfo());
    }

    socket.on('joinRoom', room => {
      socket.leave('WAITING_ROOM');
      roomInfo = joinRoom(username, room);
      socket.join(room);
      io.to(room).emit('room', roomInfo);
      socket.to('WAITING_ROOM').emit('roomsInfo', roomsInfo());
     });

    socket.on('createRoom', room => {
      if (isRoomExisted(room)){
        socket.emit('roomExisted', `Room ${room} already existed on server. Please choose another name.`);
      } else {
        createRoom(room);
        socket.leave('WAITING_ROOM');
        roomInfo = joinRoom(username, room);
        socket.join(room);
        io.to(room).emit('room', roomInfo);
        socket.to('WAITING_ROOM').emit('roomsInfo', roomsInfo());
      }
    });

    socket.on('exitRoom', room => {
      socket.leave(room);
      roomInfo = exitRoom(username, room);
      socket.join('WAITING_ROOM');
      joinRoom(username, 'WAITING_ROOM');
      if (!removeEmptyRoom(room)){
        socket.to(room).emit('room', roomInfo);
      }
      io.to('WAITING_ROOM').emit('roomsInfo', roomsInfo());
    });

    socket.on('userStatus', async ({room,status}) => {
      roomInfo = updateUserInfo(username, room, {
        name: 'isready',
        value: (status != 'true')
      });
      io.to(room).emit('room', roomInfo);

      if (isReadyToStart(roomInfo)) {
        lockRoom(room);
        socket.to('WAITING_ROOM').emit('roomsInfo', roomsInfo());
        io.to(room).emit('gamePrepare', getRandomTextId());
        await countdown(io, room, SECONDS_TIMER_BEFORE_START_GAME, 'timer', timer);
        io.to(room).emit('gameStart', 'start');
        await countdown(io, room, SECONDS_FOR_GAME, 'timeLeft', timer);
        resetUsers(room);
        unlockRoom(room);
        io.to(room).emit('gameResults', getGameResults(room));
      }
    });

    socket.on('userProgress', ({
      room,
      progress
    }) => {
      roomInfo = updateUserInfo(username, room, {
        name: 'progress',
        value: progress
      });
      io.to(room).emit('progress', roomInfo);
      if (checkIsEverybodyCompleted(room)){
        clearInterval(timer);
        resetUsers(room);
        unlockRoom(room);
        io.to(room).emit('gameResults', getGameResults(room));
      }
    })

    socket.on('userTime', ({
      room,
      time
    }) => {
      roomInfo = updateUserInfo(username, room, {
        name: 'time',
        value: SECONDS_FOR_GAME - time
      });
    })

    socket.on('disconnect', () => {
      roomInfo = userDisconnected(username);
      socket.to(roomInfo.name).emit('room', roomInfo);
      io.to('WAITING_ROOM').emit('roomsInfo', roomsInfo());
    });

  });
};

const countdown = async (io, room, startTime, message, timer) => {
  return await new Promise(resolve => {
    let i = startTime;
      timer = setInterval(() => {

      io.to(room).emit(message, i);
      i--;

      if (i < 0) {
        clearInterval(timer);
        resolve('ok');
      }
    }, 1000);
  });
}