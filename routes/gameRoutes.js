import { Router } from "express";
import path from "path";
import { HTML_FILES_PATH } from "../config";
import { getText } from '../services/textService.js';

const router = Router();

router
  .get("/", (req, res) => {
    const page = path.join(HTML_FILES_PATH, "game.html");
    res.sendFile(page);
  });

  router.get("/texts/:id", (req,res) => {
    const result = getText(req.params.id);
  if (result) {
    res.json(result);
  }});

export default router;
