import { createElement } from '../helpers/domHelper.js';
import { joinRoom,createRoom } from '../socket/socketHandler.js';

const container = document.getElementById('rooms-page');

export const renderRoomList = (roomsInfo) =>{
  container.innerHTML='';
  const roomListDiv = createRoomList(roomsInfo);
  container.append(roomListDiv);
}

const createRoomList = (rooms) => {
  const pageWrapper = createElement({ tagName: 'div', className: 'page-wrapper flex-centerd' });
  const roomListDiv = createElement({ tagName: 'div', className: 'room-list flex-centered' });
  const pageHeader = createElement({ tagName: 'h1', className: 'page-header' });
  const addBtn = createElement({ tagName: 'button', className: 'btn add-room' });

  addBtn.innerText='Create Room';
  pageHeader.innerText='Join Room Or Create New';
  
  
  rooms.forEach(room => {
    const roomDiv=createRoomDiv(room);
    roomListDiv.appendChild(roomDiv);
  });
  
  pageWrapper.append(pageHeader,addBtn,roomListDiv);
    pageWrapper.querySelector('.btn.add-room').addEventListener('click',()=>{
      const newRoom = prompt('Enter room name')
      createRoom(newRoom);
    });
  
    return pageWrapper;
}

const createRoomDiv = (room) =>{
  const roomDiv = createElement({ tagName: 'div', className: 'room' });
  const usersConnected = createElement({ tagName: 'div', className: 'users-connected' });
  const roomName = createElement({ tagName: 'div', className: 'room-name' });
  const joinBtn = createElement({ tagName: 'button', className: 'btn join' });

  usersConnected.innerText = `${room.connected} ${room.connected === 1 ? 'user' : 'users'} connected`;
  roomName.innerText=room.name;
  joinBtn.innerText='Join';
  roomDiv.append(usersConnected,roomName,joinBtn);

  roomDiv.querySelector('.btn.join').addEventListener('click',()=>{
    joinRoom(room.name);
  });
  return roomDiv;
}

