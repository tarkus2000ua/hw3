import { createElement } from './helpers/domHelper.js';
import {
  renderRoomList
} from './components/room-list.js';
import {
  renderRoom,
  createUserList
} from './components/room.js';
import {
  updateUserProgress,
  updateUserTime
} from './socket/socketHandler.js';
import { showModal } from './components/modal/modal.js';
import { exitRoom } from './socket/socketHandler.js';

const username = sessionStorage.getItem("username");
export const Player = {
  name: username,
  isready: false,
  progress: 0,
  room: 'WAITING_ROOM',
  time: 0
}

if (!username) {
  window.location.replace("/login");
}
export const socket = io("", {
  query: {
    username
  }
});

socket.on('userExisted', message => {
  alert(message);
  sessionStorage.removeItem("username");
  window.location.replace("/login");
});

socket.on('roomExisted', message => {
  alert(message);
});

socket.on('roomsInfo', roomsInfo => {
  renderRoomList(roomsInfo);
});

socket.on('room', roomInfo => {
  Player.isready = roomInfo.users.find(user => user.name === Player.name).isready;
  Player.progress = roomInfo.users.find(user => user.name === Player.name).progress;
  Player.room = roomInfo.name;
  renderRoom(roomInfo);
});

socket.on('progress', roomInfo => {
  const userList = document.querySelector('.user-list');
  const updatedUserList = createUserList(roomInfo.users);
  userList.parentNode.replaceChild(updatedUserList, userList);
});

socket.on('gamePrepare', async (textId) => {
  let response = await fetch(`/game/texts/${textId}`);
  let text = await response.text();
  const textBox = document.getElementById('text-box');
  textBox.innerHtml = '';

  text.split('').forEach(char => {
    const charSpan = document.createElement('span');
    charSpan.innerText = char;
    textBox.appendChild(charSpan);
  });

  const timer = document.getElementById('timer');
  const buttons = document.getElementsByClassName('btn');
  for (let button of buttons) {
    button.classList.toggle('hide');
  }
  timer.classList.toggle('hide');
  const timeLeft = document.getElementById('time-left');
  timeLeft.classList.toggle('hide');
});

socket.on('gameResults', results => {
  const resultsDiv = createElement({ tagName: 'div', className: 'results' });
  const resultsTable = createElement({ tagName: 'table', className: 'results-table' });
  results.forEach((result,index) => {
    const tr = createElement({ tagName: 'tr', className: 'tr' });
    const tdNumber = createElement({ tagName: 'td', className: 'td' });
    tdNumber.innerText = index+1;
    const tdName = createElement({ tagName: 'td', className: 'td' });
    tdName.innerText = result.name;
    tr.append(tdNumber,tdName);
    resultsTable.appendChild(tr)
  })
  resultsDiv.appendChild(resultsTable);
  showModal({title:'Game results:',bodyElement:resultsDiv, onClose:()=>{exitRoom(Player.room)}});
});

socket.on('timer', value => {
  const timer = document.getElementById('timer');
  timer.innerText = value
});

socket.on('timeLeft', value => {
  const timeLeft = document.getElementById('time-left');
  timeLeft.innerText = 'Time left:'+value;
  Player.time = value;
});

socket.on('gameStart', value => {
  const timer = document.getElementById('timer');
  const textBox = document.getElementById('text-box');
  timer.classList.toggle('hide');
  textBox.classList.toggle('hide');

  let text = textBox.innerText.split('');
  let progress = 0;
  const rate = 100 / text.length;
  let index = 0;

  let spans = textBox.querySelectorAll('span');
  spans[index].classList.add('next');
  window.addEventListener('keyup', event => keyUpHandler(event));

  const keyUpHandler = (e) => {
    if (e.key === text[index]) {
      spans[index].classList.remove('next');
      spans[index].classList.add('highlight');
      if (index === text.length-1) {
        progress = 100;
        updateUserProgress(Player.room, progress);
        updateUserTime(Player.room, Player.time);
        window.removeEventListener('keyup', event => keyUpHandler(event));

      } else {
        index++;
        spans[index].classList.add('next');
        progress += rate;
        // console.log(progress);
        updateUserProgress(Player.room, progress);
      }
    }
  }
});