export const texts = [
  "Text for typing #1",
  "Text for typing #2",
  "Text for typing #3",
  "Text for typing #4",
  "Text for typing #5",
  "Text for typing #6",
  "Text for typing #7",
];

export const rooms = [
  {
    "name": "WAITING_ROOM",
    "users":[]
  },
  {
  "name": "room1",
  "status":"free",
  "users": [{
    "name":"John",
    "isready": true,
    "progress":50,
    "time":100
  },
  {
    "name":"Brad",
    "isready": true,
    "progress":70,
    "time":100
  },
  {
    "name":"Ivan",
    "isready": true,
    "progress":0,
    "time":100
  }]
},
{
  "name": "room2",
  "status":"busy",
  "users": [{
    "name":"Jenny",
    "isready": false,
    "progress":0,
    "time":100
  },
  {
    "name":"Olga",
    "isready": false,
    "progress":0,
    "time":100
  },
  {
    "name":"Eve",
    "isready": false,
    "progress":0,
    "time":100
  }]
},
{
  "name": "room3",
  "status": "free",
  "users": [{
    "name":"Peter",
    "isready": true,
    "progress":100,
    "time":100
  }]
}]

export default { texts };
